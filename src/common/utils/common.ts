/**
 * 判断是否包含字符串
 * @param str 字符串
 * @param char 字符
 * return Boolean
 */
export function str_val(str: any, char: string) {
  if (typeof str !== 'string') {
    console.log('错误：这不是一个字符串')
    return false
  }
  if (str.indexOf(char) >= 0) {
    return true
  } else {
    return false
  }
}

/**
 * 深拷贝
 * @param obj 对象
 * return obj
 */
function clone(obj: any) {
  return JSON.parse(JSON.stringify(obj))
}

const DEFAULT_CONFIG = {
  id: 'id',
  children: 'children',
  pid: 'pid'
}

/**
 * 树列表转数组
 * @param tree
 * @param config
 */
export function treeToList<T = any>(tree: any, config = DEFAULT_CONFIG): T {
  const { children } = config
  const result: any = [...tree]
  for (let i = 0; i < result.length; i++) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    if (!result[i][children!]) continue
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    result.splice(i + 1, 0, ...result[i][children!])
  }
  return result
}
