import React from 'react'
import View from '@/components/View'
import { Button, List, Typography } from 'antd'
import { InfoCircleFilled } from '@ant-design/icons'
import styles from './index.module.scss'
import userIcon from '@img/icon/user/default.png'
import ImageComponent from '@/components/ImageComponent'
import { IMAGE_TYPE } from '@/common/constants'
import classnames from 'classnames'

const data = [
  {
    id: 1,
    title: '你有一个事项未处理',
    date: '2022/09/21 13:56',
    typeText: '紧急',
    type: 1
  },
  {
    id: 2,
    title: '你有一个事项未处理',
    date: '2022/09/21 13:56',
    typeText: '紧急',
    type: 1
  },
  {
    id: 3,
    title: '你有一个事项未处理',
    date: '2022/09/21 13:56',
    typeText: '普通',
    type: 0
  }
]

const Userinfo: React.FC = () => {
  return (
    <div className={styles.box}>
      <div className={styles.userinfo}>
        <View
          contentClassName={styles.header}
          border
          margin={false}
          radius={false}>
          <div className={styles.headerIcon}>
            <ImageComponent
              className={styles.icon}
              image={{
                type: IMAGE_TYPE.IMG,
                src: userIcon
              }}
            />
          </div>
          <div className={classnames(styles.headerItemBox, styles.nameBox)}>
            <div className={styles.name}>李元芳</div>
            <div className={styles.role}>集团总经理</div>
          </div>
          <div className={styles.headerItemBox}>
            <div className={styles.headerItemLabel}>代办事项</div>
            <div className={styles.headerItemValue}>12</div>
          </div>
          <div className={styles.headerItemBox}>
            <div className={styles.headerItemLabel}>今日任务</div>
            <div className={styles.headerItemValue}>16</div>
          </div>
          <div className={styles.headerItemBox}>
            <div className={styles.headerItemLabel}>我的计划</div>
            <div className={styles.headerItemValue}>7</div>
          </div>
          <div className={styles.headerItemBox}>
            <div className={styles.headerItemLabel}>完成事项</div>
            <div className={styles.headerItemValue}>10</div>
          </div>
          <div style={{ flexGrow: 1 }} />
          <div>
            <div>
              <Button type="primary">切换角色</Button>
            </div>
            <div className={styles.logout}>
              <Button type="primary" danger>
                退出登录
              </Button>
            </div>
          </div>
        </View>
        <View border title="代办事项">
          <List
            bordered={false}
            split={false}
            dataSource={data}
            renderItem={item => (
              <List.Item key={item.id}>
                <List.Item.Meta
                  avatar={
                    <InfoCircleFilled
                      style={{ color: item.type === 1 ? 'red' : 'blue' }}
                    />
                  }
                  title={item.title}
                />
                <div className={styles.date}>{item.date}</div>
              </List.Item>
            )}
          />
        </View>
      </div>
    </div>
  )
}

export default Userinfo
